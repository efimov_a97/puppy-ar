﻿using Assets.Scripts;
using CyberPuppy;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CharacterScene
{


    public class OnTouchCharacter : MonoBehaviour, IPointerClickHandler
    {
        public Animator animator;
        public ActionDefinitions Actions;

        public SoundsLibrary Sounds;
        public AudioSource audioSource;
        
        public static int score 
		{
			set{ PlayerPrefs.SetInt ("2834ujandfkajslkJR", value); }

			get{ return PlayerPrefs.GetInt ("2834ujandfkajslkJR", 0); }
		}

        private System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();

        public void OnPointerClick(PointerEventData eventData)
        {
            if ((!timer.IsRunning && timer.ElapsedMilliseconds == 0) || 
                (timer.IsRunning && timer.ElapsedMilliseconds > 1000))
            {
                timer.Stop();

                Api.Tap((response) =>
                {
                    CharacterSceneManager.Notify(GameActionId.UpdateLocalScore, response.data.points.ToString());
                    CharacterSceneManager.Notify(GameActionId.UpdateGlobalScore, response.data.totalUserPoints.ToString("D4"));
                    score = response.data.totalUserPoints;

                    if (response.data.animation != Assets.Scripts.Models.PuppyAnimation.Puppy_Sad)
                        animator.Play(response.data.animation.ToString());

                    timer.Reset();
                    timer.Start();

                }, (message) =>
                {
                    GameManager.LoadScene(GameScene.Login);
                    //CharacterSceneManager.Notify(GameActionId.UpdateLocalScore, message);
                });
            }
        }

        public void Play(string soundId)
        {

            audioSource.Stop();
            audioSource.clip = null;

            var sound = Sounds.GetSoundById(soundId);
            if (sound == null)
                Debug.Log("SoundsLibrary: not found the clip for  " + soundId);
            else
            {
                if (audioSource.clip != null)
                    Debug.Log("play sound ID = " + soundId + ", CLIP= " + audioSource.clip.name);
                //  audioSource.clip = sound;
                // audioSource.Play(1);
                audioSource.PlayOneShot(sound);
            }

        }


        public void Stop(string soundId)
        {


            var sound = Sounds.GetSoundById(soundId);
            if (sound == null)
                Debug.Log("SoundsLibrary: not found the clip for  " + soundId);
            // else
            {

                if (audioSource.clip != null)
                    Debug.Log("stop sound ID=" + soundId + ",clip = " + audioSource.clip.name);
                //if(sound.Equals(audioSource.clip))
                //if (sound.name != null && sound.name.Equals(audioSource.clip.name))
                audioSource.Stop();
            }

        }

        // Use this for initialization
        void Start()
        {


            if (animator == null)
                animator = GetComponent<Animator>();


            Actions = CharacterSceneManager.Instance.PuppyActions;
            Sounds = CharacterSceneManager.Instance.SoundsLib;
            if (audioSource == null)
                audioSource = GetComponent<AudioSource>();

        }

        public void OnActionSoundStarted(string soundId)
        {
            Debug.Log("started action" + soundId);
            Play(soundId);
        }

        public void OnActionSoundFinished(string soundId)
        {
            Stop(soundId);
            Debug.Log("finished ");
        }

        // Update is called once per frame

    }
}
