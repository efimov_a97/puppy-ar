﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CharacterScene
{
    public class Footer : MonoBehaviour
    {
        public Text FooterText;
        public int ScoreValue = 0;
        // Use this for initialization
        void Start()
        {
            if (FooterText == null)
                FooterText = GetComponentInChildren<Text>();
            
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
