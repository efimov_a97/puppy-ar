﻿using _GameSys;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sys
{

    public static class CurrentLanguage
    {
        //ISO3
        public static List<string> EnabledLanguages;

        static CurrentLanguage()
        {
            Initialize();
        }

        static void Initialize()
        {
            EnabledLanguages = new List<string>();
            string[] ids =
            {
                    "ENG",
                    "ZHO",
                    "RUS",
                    "JPN",
                    "SPA",
                    "DEU",
                    "FRA"
            };
            EnabledLanguages.AddRange(ids);
        }

        public static bool IsEnabled(string languageId)
        {
            if (string.IsNullOrEmpty(languageId))
                return false;
            string adoptedId = languageId.Trim().ToUpperInvariant();
            return EnabledLanguages.Exists(el => adoptedId.Equals(el));
        }

        public readonly static string DefaultLanguage = "ZHO";

        static string _languageId = "ENG";
        public static string Id
        {
            get { return _languageId; }
            private set { _languageId = value; }
        }

        public static void SetLanguageId(string languageId)
        {
            Id = languageId;
            GlobalEventsListener.Notify(StringDataNotificationId.SelectedLanguage, languageId);
        }
    }
}
