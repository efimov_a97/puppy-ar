﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace _GameSys
{

    public static class _Sys
    {
        public static void ResetToDefault()
        {
            _DeviceLight.ResetToDefault();
        }

        public static MapDefinitionData MapDefinition = new MapDefinitionData();
    }

    public static class _DeviceLight
    {
        public static float LightIntensity;
        public static void ResetToDefault()
        {
            LightIntensity = 2.309091f;
        }
    }

    public static class _DevicePos
    {
        public static bool IsReady { get; private set; }
        public static void SetReady() { IsReady = true; }

        public static Vector3 PlayerPosition;
        public static Quaternion PlayerRotation;

        public static Vector3 FloorPosition;
        public static Quaternion FloorRotation;


        /// <summary>
        /// main light intencity
        /// </summary>

        static _DevicePos()
        {
            ResetToDefault();
        }

       
        public static void ResetToDefault()
        {
            IsReady = false;
        }

    }


}
