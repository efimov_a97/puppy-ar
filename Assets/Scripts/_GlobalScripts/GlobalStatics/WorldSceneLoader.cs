﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _GameSys
{

    [System.Serializable]
    public class WorldSceneDef
    {
        public string WorldName;
        public string MapDbId;
        public string WorldId;
        public string BuildSettingsSceneName;
    }


    public static class WorldSceneLoader
    {
        #region To be activated externally
        public static bool AutoActivateWorldSceneOnLoad = false;
        /// <summary>
        /// List of World Ids, BuildSettings scene names etc  
        /// Required to call the scene loading with  WorldId.
        /// </summary>
        public static List<WorldSceneDef> WorldScenes = null;
        #endregion

        #region Static part
        public static string WorldId { get; private set; }
        public static bool IsWorldSceneLoaded
        {
            get { return CurScene.isLoaded; }
        }

      

        public static  WorldSceneDef GetSceneDefByWorldId(string worldId)
        {
            if (WorldScenes == null)
                Debug.LogError("WorldScenes was not assigned! It is null! Check the WorldScenes assigning game object!");
            if (string.IsNullOrEmpty(worldId))
            {
                Debug.Log("Attempts to find empty worldId! Check the WorldScenes assigning game object!"); 
            }

            WorldSceneDef ret = WorldScenes.Find(el => (el != null && worldId.Equals(el.WorldId, System.StringComparison.OrdinalIgnoreCase) ));
            return ret;
        }

        public static void LoadByWorldId(string worldId, bool unloadCurrentWorldScene)
        {

            if (WorldScenes == null)
            {
                Debug.LogError("WorldScenes is null - was not initialized!!!");
                return;
            }
            if (string.IsNullOrEmpty(worldId))
            {
                Debug.LogError("worldId is not filled - nothing to run");
                return;
            }

            if (worldId.Equals(WorldId, StringComparison.OrdinalIgnoreCase))
            {
                Debug.Log("World "+ WorldId + " Is already loaded");
                return;
            }
            var foundItem = GetSceneDefByWorldId(worldId);

            if (foundItem == null)
            {
                Debug.LogError("No "+ worldId+" was found ");
                return;
            }
            string sceneName = string.IsNullOrEmpty(foundItem.BuildSettingsSceneName) ?
                worldId : 
                foundItem.BuildSettingsSceneName;
            if (CurScene.isLoaded && unloadCurrentWorldScene)
            {
                PostponedLoadingSceneName = sceneName;
                PostponedLoadingWorldId = worldId;
                OnUnloaded += PostponedLoading;
                Unload();
            }
            else
            {
                WorldId = worldId;
                LoadBySceneId(sceneName);
                GlobalEventsListener.Notify(StringDataNotificationId.WorldSceneLoadingStarted, WorldId);
            }
        }

        static string PostponedLoadingSceneName;
        static string PostponedLoadingWorldId;
        static void PostponedLoading(string unloadingScene)
        {
            OnUnloaded -= PostponedLoading;
            Debug.Log(" PostponedLoading: loading "+ PostponedLoadingSceneName + " for world " + PostponedLoadingWorldId + "  after unloading scene " + unloadingScene);
            WorldId = PostponedLoadingWorldId;
            LoadBySceneId(PostponedLoadingSceneName);
            GlobalEventsListener.Notify(StringDataNotificationId.WorldSceneLoadingStarted, PostponedLoadingWorldId);
        }
        

        public static Scene CurScene { get; private set; }
        #region Loading and unloading functions
        static bool IsAttachedToOnSceneLoaded = false;
        static bool IsAttachedToOnSceneUnLoaded = false;

        static void LoadBySceneId(string buildSettingsSceneId)
        {
            if (!IsAttachedToOnSceneLoaded)
            {
                IsAttachedToOnSceneLoaded = true;
                SceneManager.sceneLoaded += OnSceneLoaded;
            }

            if (!IsAttachedToOnSceneUnLoaded)
            {
                IsAttachedToOnSceneUnLoaded = true;
                SceneManager.sceneUnloaded += OnSceneUnLoaded;
            }

            if (IsSceneLoadable(buildSettingsSceneId))
            {
                SceneManager.LoadScene(buildSettingsSceneId, LoadSceneMode.Additive);
                CurScene = SceneManager.GetSceneByName(buildSettingsSceneId);

              //  Debug.Log(" curscene is " + CurScene.name);
            }

        }

        static void OnSceneLoaded(Scene loadedScene, LoadSceneMode mode)
        {
            if (loadedScene.buildIndex == CurScene.buildIndex  )
            {
                GlobalEventsListener.Notify(StringDataNotificationId.WorldSceneLoaded, WorldId);
                if(AutoActivateWorldSceneOnLoad)
                    ActivateCurrentScene();
            }
           
        }

        static void OnSceneUnLoaded(Scene unloadedScene)
        {

        }

        static bool IsSceneLoadable(string sceneName)
        {
            bool rc = true;
            Scene scene = SceneManager.GetSceneByName(sceneName);
#if UNITY_EDITOR
            UnityEditor.EditorBuildSettingsScene[] scenes = null;
            if (scenes == null)
                scenes = UnityEditor.EditorBuildSettings.scenes;
            bool isSceneFound = false;
            for (int i = 0; i < scenes.Length && !isSceneFound; i++)
            {
                isSceneFound = isSceneFound ||
                    (scenes[i] != null && Path.GetFileNameWithoutExtension(scenes[i].path).Equals(sceneName, System.StringComparison.OrdinalIgnoreCase));
            }

            if (!isSceneFound)
            {
                Debug.LogError(" Scene " + sceneName + "  is invalid, possible not loaded");
                rc = false;
            }


#endif

            if (rc && scene.isLoaded)
            {
                Debug.Log("Attempt to load already loaded scene " + sceneName);
                rc = false;
            }
            return rc;

        }


        public static void Unload()
        {
            if (CurScene.isLoaded)
                SceneManager.UnloadSceneAsync(CurScene);

        }

        #endregion loading and unloading functions

        static event Action<string> OnLoaded = delegate { };

        static bool IsListensSceneManager = false;
        /// <summary>
        ///  this function have to be called by loaded object itself
        /// </summary>
        /// <param name="locationId"></param>
        public static void SetLoaded(string locationId)
        {
           if(!IsListensSceneManager)
                SceneManager.sceneUnloaded += OnSceneUnloaded;
            IsListensSceneManager = true;
            WorldId = locationId;
            OnLoaded(locationId);
        }


        public static void ActivateCurrentScene()
        {
           
            SceneManager.SetActiveScene(CurScene);
        }

        public static void SubscribeOnLoaded(Action<string> handler)
        {
            OnLoaded += handler;
        }

        public static void UnSubscribeOnLoaded(Action<string> handler)
        {
            OnLoaded -= handler;
        }

        static event Action<string> OnUnloaded = delegate  {};

        public static void SubscribeOnUnloaded(Action<string> handler)
        {
            OnUnloaded += handler;
        }

       public  static void UnSubscribeOnUnloaded(Action<string> handler)
        {
            OnUnloaded -= handler;
        }


        static void OnSceneUnloaded(Scene unloadedscene)
        {
            Debug.Log("OnSceneUnloaded " + unloadedscene.buildIndex + " current scene " + CurScene.buildIndex);
            if (unloadedscene.buildIndex == CurScene.buildIndex)
            {
                SceneManager.sceneUnloaded -= OnSceneUnloaded;
                IsListensSceneManager = false;
                OnUnloaded(WorldId);
                GlobalEventsListener.Notify(StringDataNotificationId.WorldSceneUnLoaded, WorldId);
            }
        }

        #endregion static part



    }
}
