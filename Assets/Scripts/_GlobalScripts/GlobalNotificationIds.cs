﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace _GameSys
{
    public enum SimpleNotificationId
    {

        ReInitializeEverything,
        ApplyWorldDefinitionData,
      

    }

    public enum VectorDataNotificationId
    {
        OnTableSurfaceTapped,
    }

    public enum StringDataNotificationId
    {
        WorldSceneLoadingStarted,   // parameter: WorldId
        WorldSceneLoaded,           // parameter: WorldId
        WorldSceneUnLoaded,         // parameter: WorldId
        MapDataLoadingSucceed,      // parameter: WorldId
        MapDataLoadingFailed,       // parameter: WorldId
        UnitClicked,                // parameter: Unit id
        ToggleTestMenuPanels,
        ToggleMainMenuPanels,

        SelectedLanguage
    }
}