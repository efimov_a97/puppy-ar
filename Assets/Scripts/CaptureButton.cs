﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaptureButton : MonoBehaviour {

    public GameObject ui;
    public GameObject logo;

    CaptureAndSave snapShot;

    // Use this for initialization
    void Start () {
        snapShot = GameObject.FindObjectOfType<CaptureAndSave>();
        this.GetComponent<Button>().onClick.AddListener(OnButtonClicked);
    }

    void OnEnable()
    {
        CaptureAndSaveEventListener.onError += OnError;
        CaptureAndSaveEventListener.onSuccess += OnSuccess;
    }

    void OnDisable()
    {
        CaptureAndSaveEventListener.onError += OnError;
        CaptureAndSaveEventListener.onSuccess += OnSuccess;
    }

    void OnError(string error)
    {
        Debug.Log("Error : " + error);
        ui.SetActive(true);
        logo.SetActive(false);
    }

    void OnSuccess(string msg)
    {
        Debug.Log("Success : " + msg);
        ui.SetActive(true);
        logo.SetActive(false);
    }

    public void OnButtonClicked()
    {
        ui.SetActive(false);
        logo.SetActive(true);
        snapShot.CaptureAndSaveToAlbum(ImageType.JPG);
    }
}
