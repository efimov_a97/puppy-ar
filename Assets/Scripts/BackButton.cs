﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CyberPuppy
{
    public class BackButton : MonoBehaviour
    {
        public Button button;

        // Use this for initialization
        void Start()
        {
            if (button == null)
                button = GetComponent<Button>();

            button.onClick.AddListener(OnButtonClicked);
        }

        void OnButtonClicked()
        {
            GameManager.LoadScene(GameScene.Character);
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}
