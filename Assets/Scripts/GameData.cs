﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterScene;

namespace CyberPuppy
{
    public static class GameData
    {
        public static int Credits { get; private set; }

        public static bool IsBypassStartSceneAllowed = true;
            


        public static void IncreaseCredits(int delta)
        {
            Credits = (Credits + delta < 0) ? 0 : Credits + delta;
            CharacterSceneManager.Notify(GameActionId.UpdateGlobalScore, delta.ToString());
            // CharacterSceneManager.Notify(GameActionId.ShowMessage, delta.ToString());
            Debug.Log("Credits increased to " + Credits);
        }

    }
}
