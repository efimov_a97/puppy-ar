﻿using Assets.Scripts;

namespace CyberPuppy
{
    public enum GameScene
    {
        Splash,
        Character,
        Login,
        SignUp,
        Profile,
        LeaderBoard
    }

    public class GameManager : Singleton<GameManager>
    {
        public GameScene CurrentScene = GameScene.Splash;

        void Start()
        {
            DontDestroyOnLoad(Instance);

            Invoke("DelayLoad", 2.0f);
        }

        public void DelayLoad()
        {
            LoadScene(GameScene.Character);
        }
        
        public static void LoadScene(GameScene scene)
        {
            if (Instance != null && Instance.CurrentScene != scene)
            {
                if (Api.IsAuthenticated || (scene == GameScene.SignUp || scene == GameScene.Login))
                {
                    Fader.LoadScene(scene.ToString());
                    Instance.CurrentScene = scene;
                }
                else
                {
                    Fader.LoadScene(GameScene.Login.ToString());
                    Instance.CurrentScene = GameScene.Login;
                }
            }
        }
    }
}