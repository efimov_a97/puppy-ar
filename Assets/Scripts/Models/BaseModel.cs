﻿using System;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class BaseModel<T>
    {
        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }

        public static T Parse(byte[] data)
        {
            return Parse(Encoding.UTF8.GetString(data));
        }

        public static T Parse(string json)
        {
            return JsonUtility.FromJson<T>(json);
        }
    }
}