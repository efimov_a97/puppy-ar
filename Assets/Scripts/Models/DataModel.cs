﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    public enum PuppyAnimation
    {
        //Puppy_None = 0,
        Puppy_Sad = 0,
        Puppy_Bark = 2,
        Puppy_WagTail = 3,
        Puppy_Pant = 4,
        Puppy_Frown = 5
    }

    public class DataModel : BaseModel<DataModel>
    {
        public int code;
        public string succes;
        public string error;
        public Data data;

     
    }

    [Serializable]
    public class Data
    {
        public string token;
        public PuppyAnimation animation;
        public int points;
        public int totalUserPoints;

        public User user;

        public List<Leader> leaders;
    }

    [Serializable]
    public class User
    {
        public string name;
        public string email;
        public string wallet;
        public string puppy_name;
    }

    [Serializable]
    public class Leader
    {
        public string name;
        public int points;
    }
}