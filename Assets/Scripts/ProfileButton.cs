﻿using UnityEngine;
using UnityEngine.UI;

namespace CyberPuppy
{
    public class ProfileButton : MonoBehaviour
    {
        public Button button;

        // Use this for initialization
        void Start()
        {
            if (button == null)
                button = GetComponent<Button>();

            button.onClick.AddListener(OnButtonClicked);
        }

        void OnButtonClicked()
        {
            GameManager.LoadScene(GameScene.Profile);
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}
