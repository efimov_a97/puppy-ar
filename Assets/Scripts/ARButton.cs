﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARButton : MonoBehaviour {

    public GameObject backImage;
    public GameObject staticBackground;

    // Use this for initialization
    void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(OnButtonClicked);
    }

    public void OnButtonClicked()
    {
        backImage.SetActive(!backImage.active);
        staticBackground.SetActive(!staticBackground.active);
    }

}
